import {getBUMetaModel, getEmployeesMetaModel} from "./getMeta";

export {
    getEmployeesMetaModel, getBUMetaModel
};

export default getEmployeesMetaModel;
