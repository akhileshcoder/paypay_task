import {getDataBase, runMigration} from "../dbUtils";

export const bootstrapModel = (callback) => {
    (async () => {
        await getDataBase();
        await runMigration();
        return callback();
    })();
};
