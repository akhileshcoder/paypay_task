import {getDataBase, database} from "../dbUtils";

const cleanStr = (field:string) => (`'${(field + '').split(/[^0-9a-zA-Z,-_\/\\\s\t]/).join(' ')}'`);
const cleanInt = (field:number|string) => parseInt(field+'', 10) || 0;

const getQuery = ({NAME,EMAIL,PASSWORD,PICTURE_URL,JOB,MANAGER,SALARY,BU_NO,GRADE,TYPE, AUTH_TYPE,ABOUT}) =>
    `INSERT INTO ${database}.EMPLOYEE (NAME,EMAIL,PASSWORD,PICTURE_URL,JOB,MANAGER,HIRED_ON,SALARY,BU_NO,GRADE,TYPE,AUTH_TYPE,ABOUT) VALUES `+
 `(${cleanStr(NAME)},${cleanStr(EMAIL)},${cleanStr(PASSWORD)},${cleanStr(PICTURE_URL)},${cleanStr(JOB)},`+
 `${cleanInt(MANAGER)}, curdate(),${cleanInt(SALARY)},${cleanInt(BU_NO)},${cleanInt(GRADE)},${cleanStr(TYPE)},${cleanStr(AUTH_TYPE)},${cleanStr(ABOUT)})`;


export async function addEmployeeModel (data): Promise<any> {
    const db = await getDataBase();
    console.log('data: ', JSON.stringify(data));
    return await db.query(getQuery(data));
}
