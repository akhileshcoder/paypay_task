import {addEmployeeModel} from "./addEmployee";
import {getEmployeesModel} from "./getEmployees";

export {
    addEmployeeModel, getEmployeesModel
};

export default getEmployeesModel;
