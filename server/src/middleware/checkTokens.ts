import {Request, Response} from "express";

import {verify} from "jsonwebtoken";
import {config} from "../config/configs"

export const checkTokens = (req: Request, res: Response, next) => {
    let method = req.method, data;
    if(['get', 'head'].includes(method.toLowerCase())){
        data = req.query;
    } else {
        data = req.body;
    }
    const {token, ...rest} = data;
    req.query = rest;
    req.body = rest;

    verify(token, config.token_secret_key, (err, decoded)=> {
        console.log({decoded});
        if(err) {
            res.status(404).send({message: 'Token expired'})
        } else {
            next();
        }
    });
};
