const express = require("express"),
  path = require("path"),
  fs = require("fs"),
  app = express();
import {createMiddleware} from "./middleware"
import {createRoutes} from "./routes/createRoutes";
import {config} from "./config/configs";
import {bootstrapModel} from "./model"

const publicPath = path.resolve(__dirname, "../../client/dist");

bootstrapModel(() => {
  createMiddleware(express, app, publicPath);
  createRoutes(app);

  var server = app.listen(config.port, function() {
    console.log("listening on port : ", config.port);
    console.log("publicPath: ", publicPath);
  });
});
