CREATE TABLE IF NOT EXISTS REVIEW (
  ID INT AUTO_INCREMENT,
  GIVEN_BY INT,
  GIVEN_TO INT,
  STAR INT,
  REVIEW_L1 VARCHAR(500),
  REVIEW_L2 VARCHAR(500),
  REVIEW_L3 VARCHAR(500),
  REVIEWED_ON DATE,
  constraint pk_review primary key (ID),
  constraint fk_employee_by foreign key (GIVEN_BY) references EMPLOYEE (ID),
  constraint fk_employee_to foreign key (GIVEN_TO) references EMPLOYEE (ID)
)
