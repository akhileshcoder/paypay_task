const path = require('path');

module.exports = ctx => {
  console.log('cwd: ', ctx.cwd);
  return {
    plugins: [
      require("autoprefixer"),
      require("postcss-import"),
      require("postcss-simple-vars")({
        variables: require('./src/styles/variables.json')
      }),
      require("postcss-extend"),
      require("postcss-nested"),
      require("postcss-mixins"),
    ],
    "local-plugins": true
  }
};
