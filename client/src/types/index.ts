interface Entity {
  name: string
}

export interface IReview {
  ID: number,
  GIVEN_BY: number,
  GIVEN_TO: number,
  STAR: number,
  REVIEW_L1: string,
  REVIEW_L2: string,
  REVIEW_L3: string,
  REVIEWED_ON: string
}
export interface IEmployee {
  ID: number,
  NAME: string,
  EMAIL: string,
  PASSWORD: string,
  JOB: string,
  MANAGER: number,
  HIRED_ON: string,
  SALARY: number,
  BU_NO: number,
  GRADE: number,
  TYPE: 'EXTERNAL' | 'INTERNAL' | 'CONTRACT' |  'CLIENT',
  AUTH_TYPE: 'ADMIN' | 'EMPLOYEE',
  PICTURE_URL: string,
  ABOUT: string
}

export const defaultIEmployee:IEmployee =  {
  ID: 1,
  NAME: 'Wolverine',
  EMAIL: '',
  PASSWORD: '',
  JOB: 'JOB_0',
  MANAGER: 11,
  HIRED_ON: '2017-06-15',
  SALARY: 5786,
  BU_NO: 11,
  GRADE: 3,
  TYPE: 'EXTERNAL',
  AUTH_TYPE: 'ADMIN',
  PICTURE_URL: '/images/employee.svg',
  ABOUT: 'ABOUT me'
};

export interface IManufacturer {
  models: Entity []
  name: string
}

export type IFiltersKeys = 'ID' | 'NAME'
    | 'JOB' | 'MANAGER' | 'HIRED_ON'
    | 'SALARY' | 'BU_NO' | 'GRADE' | 'AUTH_TYPE' | 'TYPE';

export type Ifilters = {
  ID?: [number],
  NAME?: [string],
  JOB?: [string],
  MANAGER?: [number],
  HIRED_ON?: [string],
  SALARY?: [number],
  BU_NO?: [number],
  GRADE?: [number],
  TYPE?: ['EXTERNAL' | 'INTERNAL' | 'CONTRACT' |  'CLIENT']
  AUTH_TYPE?: ['ADMIN' | 'EMPLOYEE']
}

export const selectedFilters = {
  ID: '',
  NAME: '',
  JOB: '',
  MANAGER: '',
  HIRED_ON: '',
  SALARY: '',
  BU_NO: '',
  GRADE: '',
  TYPE: '',
  AUTH_TYPE: ''
};

export interface IEmpResponse {
  employee: Array<IEmployee>
  filters: Ifilters
  totalEmployeeCount: number
  totalPageCount: number
}

export interface filter {
  value: string | {manufacturerName: string, name: string, uuid: string},
  formatter: Function,
  url: string,
  name: string,
  label: string,
  loadingText: string
}

export type manufacturer = {
  manufacturers: Array<{
    name: string,
    uuid:string,
    models: Array<{name:string, uuid:string}>
  }>
};

export interface newReview {
  STAR: number,
  REVIEW_L1: string,
  REVIEW_L2: string,
  REVIEW_L3: string,
}

export const signInFields = {
  EMAIL: {
    type: 'email',
    value: ''
  },
  PASSWORD: {
    type: 'password',
    value: ''
  }
};

export const employeeFormFields = {
  NAME: {
    type: 'text',
    value: ''
  },
  EMAIL: {
    type: 'email',
    value: ''
  },
  PASSWORD: {
    type: 'password',
    value: ''
  },
  PICTURE_URL: {
    type: 'text',
    value: 'http://139.99.119.8/static/images/Akhilesh.jpg'
  },
  JOB: {
    type: 'text',
    value: ''
  },
  MANAGER: {
    type: 'select',
    options: [],
    value: ''
  },
  SALARY: {
    type: 'number',
    value: 10
  },
  BU_NO: {
    type: 'select',
    options: [],
    value: ''
  },
  GRADE: {
    type: 'number',
    value: 3,
    min: 3,
    max: 13
  },
  TYPE: {
    type: 'select',
    options: ['EXTERNAL', 'INTERNAL', 'CONTRACT', 'CLIENT'].map(i=>({label:i, uuid: i})),
    value: 'CLIENT'
  },
  AUTH_TYPE: {
    type: 'select',
    options: ['ADMIN', 'EMPLOYEE'].map(i=>({label:i, uuid: i})),
    value: 'ADMIN'
  },
  ABOUT: {
    type: 'textarea',
    value: ''
  },
};
