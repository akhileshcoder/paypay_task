import React, { Component } from 'react';
import {PageRoute} from "./route";
import {fetchMeta} from "./utils";

export interface Props {
}

export class AppMain extends Component<Props> {
  _isMounted = false;
  state = {
    isNativeAssetsLoaded: false
  };
  render() {
    const {
      isNativeAssetsLoaded
    } = this.state;
    isNativeAssetsLoaded && console.log('all assets fetched:')
    return (
        isNativeAssetsLoaded ? <PageRoute /> : <div className="loader">Loading...</div>
    );
  }
  componentDidMount() {
    this._isMounted = true;
    fetchMeta().then(r => {
      this._isMounted && this.setState({isNativeAssetsLoaded:true});
    });

  }
  componentWillUnmount() {
    this._isMounted = false;
  }
}

export default AppMain;
