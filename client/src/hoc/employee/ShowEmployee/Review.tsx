import React, { Fragment, PureComponent } from 'react';
import {Card, Img, Button, Chip} from '../../../design';
import { IReview } from '../../../types';
import {getMeta, i18n, timeDiff} from "../../../utils";

export interface EmployeeDetails {
  totalEmployeeCount: number,
  totalPageCount: number
  employee: Array<IReview>
}

export interface Props {
  data?: {filteredReviews: Array<IReview>}
}
const getReviewTile = (review: IReview, key:number) => {
  const {ID, GIVEN_BY, GIVEN_TO, REVIEW_L1, REVIEW_L2, REVIEW_L3, REVIEWED_ON, STAR} = review;
  const starDiv = [1,2,3,4,5].map((i,ind)=>(ind< STAR ? <b className="star start-b" key={ind}>★</b> : <span className="star" key={ind}>☆</span>));
  return (
      <div className="review-tile" key={key}>
        <Card title={ <Fragment>{starDiv}</Fragment>} body={
          <Fragment>
            <Chip title={i18n("GIVEN_BY")} body={getMeta('GIVEN_BY', GIVEN_BY, true)}/>
            <Chip title={i18n("GIVEN_TO")} body={getMeta('GIVEN_TO', GIVEN_TO, true)}/>
            <Chip icon="/images/cal.png" body={timeDiff(REVIEWED_ON)}/>
          </Fragment>
        } isBorderLess={true}/>
        <Card
            title={ <Fragment><b>{i18n('REVIEW_L1')}</b> <span>{REVIEW_L1}</span></Fragment> }
            body={<Fragment><b>{i18n('REVIEW_L2')}</b> <span>{REVIEW_L2}</span></Fragment>}
            footer={<Fragment><b>{i18n('REVIEW_L3')}</b> <span>{REVIEW_L3}</span></Fragment>}
            isBorderLess={true}/>
      </div>
  );
};
export class Review extends PureComponent<Props> {
  render() {
    const {data} = this.props;
    return (
        <Fragment>
            {data && data.filteredReviews.length ? data.filteredReviews.map((review:IReview, index)=>getReviewTile(review, index)) : <div className="review-tile">
                <Card title={<h2>{i18n('No review available')}</h2>} isBorderLess={true}/>
            </div>}
        </Fragment>
    )
  }
}

