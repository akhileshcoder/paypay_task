import {EmployeeDetails, Employee} from './Employee';
import {ShowEmployee} from './ShowEmployee';

export {EmployeeDetails, ShowEmployee, Employee}

export default Employee;
