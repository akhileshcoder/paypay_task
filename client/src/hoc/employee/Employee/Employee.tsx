import React, { Fragment, PureComponent } from 'react';
import {Card, Img, Button, Chip, JsonTester} from '../../../design';
import { IEmployee } from '../../../types';
import { ShowEmployee } from '../ShowEmployee';
import {timeDiff, getMeta, i18n} from "../../../utils";

export interface EmployeeDetails {
  totalEmployeeCount: number,
  totalPageCount: number
  employee: Array<IEmployee>
}

export interface Props {
  details: IEmployee
}
export class Employee extends PureComponent<Props> {
  _isMounted = false;
  state = {
    isPreview: false
  };
  render() {
    const {details} = this.props;
    const {ID, PICTURE_URL, HIRED_ON, NAME, ABOUT, ...chipItems} = details;
    return (
      this.state.isPreview ? (<ShowEmployee details={details} onClose={this.togglePreview}/>) : (
        <div className="employee-tile">
          <Img src={PICTURE_URL} alt={NAME}/>
          <Card
            title={NAME}
            body={
              <Fragment>
                {
                  Object.entries(chipItems).map(([title, body], index) =>(<Chip key={index} title={i18n(title)} body={i18n(getMeta(title, body, true))}/>))
                }
                <Chip icon="/images/cal.png" body={timeDiff(HIRED_ON)} />
              </Fragment>
            }
            footer={<Button className="tile-action" onClick={this.togglePreview} isText={true}> {i18n('View details')} </Button>}
            isBorderLess={true}/>
        </div>
      )
    );
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  togglePreview = () => {
    this._isMounted && this.setState({isPreview: !this.state.isPreview});
  }
}

