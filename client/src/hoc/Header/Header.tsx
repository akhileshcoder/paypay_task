import React, {PureComponent} from 'react';
import {NavLink as Link} from 'react-router-dom';
import {Button, Logo} from '../../design';
import {getToken, getUserDet, i18n} from "../../utils"

export interface Props {
}

export class Header extends PureComponent<Props> {
    render() {
        let pages = [
            {link: `/i-am-a-random-url`, label: i18n('Random')}
        ];
        const userDet = getUserDet();
        if (getToken()) {
            pages = [...pages, {link: '/', label: i18n('Dashboard')}];
            if (userDet.AUTH_TYPE === 'ADMIN') {
                pages.push({link: '/add-employee', label: i18n('Add employee')})
            }
            pages = [...pages, {link: '/logout', label: i18n('Logout')}]
        } else {
            pages = [...pages, {link: '/login', label: i18n('Login')}]
        }
        return <div className="header">
            <Logo img="/images/logo.png"/>
            <div className="header-r">
                {
                    pages.reverse().map((page, ind) => <Button className="pull-right" key={ind} isText={true}>
                        <Link key={ind} to={page.link}>
                            {page.label}
                        </Link>
                    </Button>)
                }
            </div>
        </div>;
    }
}
