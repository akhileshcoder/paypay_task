import {Request} from "./request";
import {Tooltip} from "../design";
import React from "react";
import {i18n} from "./i18n"

const type = {employee: 'employee', bu: 'bu'};
const titleMap = {
    MANAGER: type.employee,
    GIVEN_BY: type.employee,
    GIVEN_TO: type.employee,
    BU_NO: type.bu,
};

let metadata: any = {employee: {}, bu: {}};

export const fetchMeta = async () => {
    metadata = await new Request('/api/meta');
    return metadata;
};

export const getMeta = (title: string | number, ID: string | number, wrapElement?: boolean) => {
    // @ts-ignore
    const data = metadata[titleMap[title]] && metadata[titleMap[title]][ID] ? metadata[titleMap[title]][ID] : ID;
    // @ts-ignore
    if (wrapElement && titleMap[title]) {
        const {PICTURE_URL, ...kys} = data;
        return <Tooltip message={
            <ul className="menu">
                {
                    Object.entries(kys).map(([k,v], ind)=><li key={ind} className="menu-item"><b>{i18n(k)}</b>: {v}</li>)
                }
            </ul>
        } position={'bottom'}>
            <div>{kys.NAME || kys}</div>
        </Tooltip>
    } else {
        return (data && data.NAME) || data || ID;
    }
};
 export const getMetaData = () => JSON.parse(JSON.stringify(metadata));
