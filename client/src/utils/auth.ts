import {IEmployee} from "../types";
const localStorageKey = 'userCred';

let locTimer: any;
const timer_interval = 1000;
const assignLocation = (dest: string) =>{
    (window.location.href !== dest) && window.location.assign(dest);
};
export const makeLogin  = (creds:any) => {
    localStorage.setItem("userCred", JSON.stringify(creds));
    clearTimeout(locTimer);
    locTimer = setTimeout(()=> assignLocation(location.origin), timer_interval);
};

export const makeLogout = () => {
    localStorage.clear();
    clearTimeout(locTimer);
    locTimer = setTimeout(()=> assignLocation(location.origin+'/login'), timer_interval);
};

export const getToken = () => {
    let userCred:any = localStorage.getItem(localStorageKey), token;
    userCred && (token = JSON.parse(userCred).token);
    if(!token) {
        makeLogout();
    }
    return token
};

export const getUserDet = () => {
    let userCred:any = localStorage.getItem(localStorageKey), userDet: IEmployee;
    if(userCred) {
        userDet = JSON.parse(userCred).userDet;
    }
    // @ts-ignore
    if(!userDet) {
        makeLogout();
    }
    // @ts-ignore
    return userDet || {}
};
