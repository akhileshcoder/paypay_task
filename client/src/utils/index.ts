import {Method, Request, RequestError, RequestResponse} from './request';
import {compare} from './object';
import {timeDiff} from './time';
import {fetchMeta, getMeta, getMetaData} from './meta';
import {getToken, makeLogin, makeLogout, getUserDet} from "./auth"
import {i18n} from "./i18n"

export {
    Method,
    Request,
    RequestError,
    RequestResponse,
    compare,
    timeDiff,
    getMeta,
    fetchMeta,
    getMetaData,
    makeLogin,
    makeLogout,
    getToken,
    getUserDet,
    i18n
}

export default Request;
