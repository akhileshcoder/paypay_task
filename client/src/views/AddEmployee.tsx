import React, {Component} from 'react';

import {Button, GenericInput, Select, TextareaInput} from "../design"
import {employeeFormFields} from "../types"
import {getMetaData, getUserDet, i18n, makeLogout, Request} from "../utils"

interface Props {

}

const getFormFields = (metadata: any, defaultFormField: any) => {
    defaultFormField = JSON.parse(JSON.stringify(defaultFormField));
    // @ts-ignore
    defaultFormField.MANAGER.options = Object.entries(metadata.employee).map(([k, {NAME, TYPE, BU_NO}]) => ({
        label: `${NAME}(${TYPE}), ${metadata.bu[BU_NO].NAME}`,
        uuid: k
    }));
    defaultFormField.MANAGER.value = defaultFormField.MANAGER.options[0].uuid;

    // @ts-ignore
    defaultFormField.BU_NO.options = Object.entries(metadata.bu).map(([k, {NAME, LOCATION}]) => ({
        label: `${NAME}(${LOCATION})`,
        uuid: k
    }));
    defaultFormField.BU_NO.value = defaultFormField.BU_NO.options[0].uuid;
    return defaultFormField;
};

export class AddEmployee extends Component<Props> {
    state: { formFields?: any, metaData?: any } = {};

    constructor(props: Props) {
        super(props);
        const metaData = getMetaData();
        this.state = {
            metaData, formFields: getFormFields(metaData, employeeFormFields)
        };
    }

    getFormItems = ([k, {type, value, options}]: any, index: string | number | undefined) => {
        switch (type) {
            case 'select':
                return <Select hideEmpty={true} key={index} label={i18n(k)} name={k} onChange={this.onChange}
                               options={options} selectedOption={value}/>;
            case 'textarea':
                return <TextareaInput key={index} label={i18n(k)} name={k} onChange={this.onChange}/>;
            default:
                return <GenericInput key={index} label={i18n(k)} type={type} name={k} value={value}
                                     onChange={this.onChange}/>;
        }
    };

    render() {
        const {formFields} = this.state;
        return (
            <div className="form-horizontal">
                {
                    formFields ? Object.entries(formFields).map(this.getFormItems) : null
                }
                <Button disabled={!!Object.values(formFields).find(({value}) => !value)} fullWidth={true}
                        onClick={this.onSubmitChange}> Add Employee</Button>
            </div>
        )
    }
    componentDidMount(): void {
        const userDet = getUserDet();
        if(userDet.AUTH_TYPE !== 'ADMIN') {
            makeLogout();
        }
    }

    onChange = (name: string, value: string) => {
        const {formFields} = this.state;
        formFields[name].value = value;
        this.setState({formFields});
    };
    onSubmitChange = () => {
        const {formFields} = this.state;
        console.log('formFields', this.state.formFields);
        (async () => {
            const res = await new Request('/api/add-employee', 'POST', Object.entries(formFields).reduce((acc, [k,v])=>{
                // @ts-ignore
                acc[k] = v.value;
                return acc;
            },{}));
            if(res) {
                const metaData = getMetaData();
                this.setState({
                    metaData, formFields: getFormFields(metaData, employeeFormFields)
                });
            }
        })();
    }

}

export default AddEmployee
