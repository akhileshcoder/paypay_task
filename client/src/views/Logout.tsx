import React, { Component, Fragment } from 'react';
import { Logo } from '../design/Logo';
import { Button } from '../design/Button';
import { EmptyState } from '../design/empty-state';
import {i18n, makeLogout} from "../utils";
interface Props {

}
export class Logout extends Component<Props> {
  render () {
    return (
      <EmptyState
        emptyIcon={
          <Logo img="/images/logo.png" />
        } title={i18n("Are you sure?")}
        subtitle={i18n("You might loose your local state.!")}
        action={
          <Fragment>
            <span>{i18n('Ok if you wish')}, </span>
            <Button isText={true} onClick={makeLogout}> {i18n("Let's Logout")} </Button>
          </Fragment>
        }
      />
    )
  }
}

export default Logout
