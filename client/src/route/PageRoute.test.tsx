import React from "react";
import { render, fireEvent, waitForElement, cleanup, findAllByText } from '@testing-library/react';

import {PageRoute, Props} from "./PageRoute";

describe("<PageRoute />", () => {
  function renderPageRoute(props: Partial<Props> = {}) {
    const defaultProps = {  };
    const renderedElm = render(<PageRoute {...defaultProps} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderPageRoute();
  });
});
