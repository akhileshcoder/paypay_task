import React, { Component, Fragment } from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
    NavLink as Link
} from 'react-router-dom'

import {Dashboard, AddEmployee, NotFound, Login, Logout} from '../views';
import { Header, Footer } from '../hoc';

export interface Props {

}

export class PageRoute extends Component<Props> {
    render () {
        return (
          <BrowserRouter>
              <Fragment>
                  <Header />
                  <Switch>
                      <Route exact path="/" component={Dashboard} />
                      <Route path="/add-employee" component={AddEmployee} />
                      <Route path="/logout" component={Logout} />
                      <Route path="/login" component={Login} />
                      <Route path="*" component={NotFound} />
                  </Switch>
                  <Footer/>
              </Fragment>
          </BrowserRouter>
        )
    }
}

export default PageRoute
