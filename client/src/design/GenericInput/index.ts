import {GenericInput} from './GenericInput';
import {TextareaInput} from './TextareaInput';

export {GenericInput, TextareaInput}

export default GenericInput;
