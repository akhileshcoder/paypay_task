import { cleanup, render } from '@testing-library/react';
import { Chip, Props } from './Chip';
import React, {PureComponent} from 'react';

describe('<Card />', () => {
  function renderCard(props: Partial<Props> = {}) {
    const renderedElm = render(<Chip {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderCard({
      title: 'title',
      body: 'body',
    });
  });
});
