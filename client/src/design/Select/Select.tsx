import React, {FormEvent, PureComponent} from 'react';

export type optionType = number | string | {label: string, uuid: string, disabled?:boolean};

export const defaultOption = {label: '', uuid: ''};

export interface Props {
    label?: string
    name: string
    multi?: boolean
    hideEmpty?: boolean
    options?: Array<optionType>
    selectedOption?: optionType
    onChange: Function
    formatter?: (value: string | optionType, index: number, array: (string | optionType)[]) => optionType
}

export class Select extends PureComponent<Props> {
    _isMounted = false;

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount(): void {
        this._isMounted = true;
    }

    state:{selectedOption: optionType, options: Array<optionType>}= {
        selectedOption: this.props.selectedOption || '',
        options: [defaultOption]
    };

    constructor(props: Props) {
        super(props);
        let {selectedOption, options, formatter} = this.props;
        options = options ? (formatter ? options.map(formatter) : options) : [];
      this.state = {
            selectedOption: selectedOption || '', options
        };
    }

    render() {
        const {label, hideEmpty} = this.props;
        let {selectedOption, options} = this.state;
        if(!hideEmpty) {
            // @ts-ignore
            const emptyElm = options.find((i: optionType)=> !i || (i && !i.uuid));
            emptyElm || (label && options.unshift({label: `All ${label}`, uuid: ''}));
        }
        return (
            <div className="form-group">
                <label>
                    {
                        label ? <div className="form-label">{label}</div> : null
                    }
                    <select className="form-select"
                            value={
                              typeof selectedOption === 'string' || typeof selectedOption === 'number'
                                ? selectedOption : selectedOption.uuid
                            }
                            onChange={this.handleChange}>
                        {
                            options.map(
                                (option, ind) =>
                                    <option key={ind} value={
                                      typeof option === 'string' || typeof option === 'number' ? option : option.uuid
                                    }> {typeof option === 'string' || typeof option === 'number' ? option : option.label} </option>
                            )
                        }
                    </select>
                </label>
            </div>
        );
    }

    handleChange = (event: FormEvent) => {
        const {options} = this.state;
        // @ts-ignore
        const {value} = event.target;
        this._isMounted && this.setState({selectedOption: value});
        this.props.onChange(this.props.name, options.find(option => {
          if(typeof option === 'string' || typeof option === 'number') {
            return option === value
          } else {
            return option.uuid === value
          }
        }));
    };
}
